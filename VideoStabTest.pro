QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TEMPLATE = app
CONFIG += c++11

LIBS += -L/usr/local/lib -lopencv_core -lopencv_imgcodecs -lopencv_highgui -lopencv_imgproc \
        -lopencv_flann -lopencv_videostab -lopencv_features2d -lopencv_xfeatures2d \
        -lopencv_calib3d -lopencv_videoio -lopencv_video

SOURCES += VideoStabilizer.cpp \
    Measurement.cpp \
    StabilizationForm.cpp \
    Utility.cpp \
    qcustomplot.cpp \
    graphwindow.cpp \
    genericdetector.cpp

HEADERS += \
    VideoStabilizer.h \
    Measurement.h \
    StabilizationForm.h \
    Utility.h \
    qcustomplot.h \
    graphwindow.h \
    genericdetector.h

FORMS += \
    stabilizationform.ui \
    graphwindow.ui

RESOURCES += \
    videostab.qrc

