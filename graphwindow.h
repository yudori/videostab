#ifndef GRAPHWINDOW_H
#define GRAPHWINDOW_H

#include <QString>
#include <QMainWindow>


namespace Ui {
class GraphWindow;
}

class GraphWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit GraphWindow(QWidget *parent = 0);
    void addComponent(QString &name, QVector<double> vals);
    void setLabels(const QString x, const QString y);
    void setRange(double xLower, double xUpper, double yLower, double yUpper);
    ~GraphWindow();

private:
    int numberOfGraphs;
    int frameLength = 0;
    Ui::GraphWindow *ui;
    QVector<QColor> colors;
};

#endif // GRAPHWINDOW_H
