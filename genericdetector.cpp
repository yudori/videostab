#include "genericdetector.h"

GenericDetector::GenericDetector(QString &name)
{
    this->name = name;
}

QString& GenericDetector::getName(){
    return name;
}

cv::Ptr<cv::FeatureDetector> GenericDetector::getDetector(){
    if (name == "SIFT")
    {
        return cv::xfeatures2d::SIFT::create();
    }
    if (name == "SURF")
    {
        return cv::xfeatures2d::SurfFeatureDetector::create(400);
    }
    if (name == "ORB")
    {
        //cv::Ptr<cv::FeatureDetector> orb = cv::ORB::create();

        return cv::ORB::create();
    }
    if (name == "BRISK")
    {
        return cv::BRISK::create();
    }
    return cv::Ptr<cv::FeatureDetector>();
}

cv::BFMatcher* GenericDetector::getMatcher(){
    int type = cv::NORM_L2;
    if (name == "ORB" || name == "BRISK")
    {
        type = cv::NORM_HAMMING;
    }
    return new cv::BFMatcher(type);
}
