#include <sstream>
#include <iomanip>
#include <QDebug>

#include "VideoStabilizer.h"
#include "Measurement.h"
#include "Utility.h"
#include "AlgorithmFactory.h"

using namespace cv;
using namespace cv::xfeatures2d;


VideoStabilizer::VideoStabilizer(QString inputVideoPath, QObject *parent) :
    QObject(parent),
    resizeFactor(0.6),
    speed(30),
    graphView(0),
    originalOutput(0),
    stabilizedOutput(0){
    setVideoPath(inputVideoPath);
    setDetector("SURF");
}

void VideoStabilizer::setVideoPath(QString path){
    this->inputVideoPath = path;
}

// Removes all the black pixels found on frames collected
void crop(cv::Mat& image)
{
    cv::Mat gray;
    cv::cvtColor(image, gray, CV_RGB2GRAY);
    int minCol = gray.cols;
    int minRow = gray.rows;
    int maxCol = 0;
    int maxRow = 0;
    for (int i = 0; i < gray.rows - 3; ++i)
    {
        for (int j = 0; j < gray.cols; ++j)
        {
            if (gray.at<char>(i, j) != 0)
            {
                if (i < minRow) minRow = i;
                if (j < minCol) minCol = j;
                if (i > maxRow) maxRow = i;
                if (j > maxCol) maxCol = j;
            }
        }
    }
    cv::Rect cropRect(minCol, minRow, maxCol - minCol, maxRow - minRow);
    image = image(cropRect).clone();
}


const char* VideoStabilizer::stabilize(){

    qDebug() << inputVideoPath;
    cap.open(inputVideoPath.toStdString());
    //cap.open("input.mp4");

    frameLength = cap.get(CV_CAP_PROP_FRAME_COUNT);
    psnrV = QVector<double>(frameLength - 1);
    if (!cap.isOpened()){
        throw "Error! failed to read Video file...";
    }

    cap >> curr_frame;
    //detector = SurfFeatureDetector::create();

    resize(curr_frame, curr_frame, Size(curr_frame.cols * resizeFactor, curr_frame.rows * resizeFactor));
    detector->getDetector()->detectAndCompute( curr_frame, Mat(), curr_keypoints, curr_descriptors );
    curr_frame_corrected = curr_frame.clone();
    kf = cv::KalmanFilter(8, 8, 0, CV_64F);
    cv::setIdentity(kf.measurementMatrix);

    // The initial homography is the identity transformation
    previousH = cv::Mat(3, 3, CV_64F);
    cv::setIdentity(previousH);

    kfInitialized = false;
    frameCount = 0;
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(speed);


}

void VideoStabilizer::update(){

    if (frameCount >= frameLength - 1){
        qDebug() << "EUREKA!";
        if (graphView != 0){
            graphView->addComponent(detector->getName(), psnrV);
            qDebug() << "we have a graph view";
        }
        timer->stop();
        return;
    }
    prev_frame = curr_frame.clone();
    //curr_frame.copyTo(prev_frame);
    prev_keypoints = curr_keypoints;
    prev_descriptors = curr_descriptors.clone();

    cap >> curr_frame;

    resize(curr_frame, curr_frame, Size(curr_frame.cols * resizeFactor, curr_frame.rows * resizeFactor));

    curr_keypoints.clear();
    curr_descriptors = Mat();

    if( !prev_frame.data || !curr_frame.data )
    { std::cout<< " --(!) Error reading images " << std::endl;  }

    detector->getDetector()->detectAndCompute( curr_frame, Mat(), curr_keypoints, curr_descriptors );

    // We havent retrieved enough keypoints
    if (curr_keypoints.size() < 20) {
        std::cout<< " --(!) Keypoints < 20 " << std::endl;
        timer->start();
    }

    detector->getMatcher()->match( curr_descriptors, prev_descriptors, matches );

    double minDistance = 100;
    for (unsigned int i = 0; i < matches.size(); i++) {
        if (matches[i].distance < minDistance) {
            minDistance = matches[i].distance;
        }
    }

    prev_points.clear();
    curr_points.clear();

    //-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )

    for (unsigned int i = 0; i < matches.size(); i++) {
        if (matches[i].distance
                < std::max(25.0, 7.0 * minDistance)) {
            curr_points.push_back(curr_keypoints[matches[i].queryIdx].pt);
            prev_points.push_back(prev_keypoints[matches[i].trainIdx].pt);
        }
    }

    if (prev_points.size() <=10 || curr_points.size() <=10){
        timer->start();
    }
    Mat H = findHomography( prev_points, curr_points, RANSAC );

    // Initialize the Kalman filter state
    // Only performed the first time an homography is calculated
    if (!kfInitialized) {
        kf.statePre = H.reshape(1, 9).rowRange(0, 8).clone();
        kf.statePost = H.reshape(1, 9).rowRange(0, 8).clone();
        kfInitialized = true;
    }

    if (H.cols < 3){
        std::cout << "Homography calculation failed" << std::endl;
        timer->start();
    }

    // Kalman filter loop: predict and correct
    kf.predict();
    qDebug() << H.rows << "---" << H.cols << "---" << H.size;
    cv::Mat HEstimated = kf.correct(H.reshape(1, 9).rowRange(0, 8));
    HEstimated.push_back(cv::Mat(1, 1, CV_64F, cv::Scalar(1)));

    HEstimated = HEstimated.reshape(1, 3);

    // This controls the ammount of smoothing
    // The more weight previousH has, the more stable the footage will be
    cv::addWeighted(previousH, 0.8, cv::Mat::eye(3, 3, CV_64F),
            1 - 0.8, 0, previousH);

    cv::Mat HEstimated_ptsPrevious; // H_smoothed * previous_points
    cv::Mat HPrevious_ptsPrevious; //  H_used_to correct_previous_frame * previous_points

    // The homography between:
    // The previous points transformed with the smoothed homography and
    // The prevous points transformed with the previous correction homography
    // Is the homography that corrects the current frame

    cv::perspectiveTransform(prev_points, HEstimated_ptsPrevious,
            HEstimated);
    cv::perspectiveTransform(prev_points, HPrevious_ptsPrevious, previousH);

    cv::Mat HCorrection = cv::findHomography(HEstimated_ptsPrevious,
            HPrevious_ptsPrevious, cv::RANSAC);

    cv::warpPerspective(curr_frame, curr_frame_corrected, HCorrection,
            cv::Size(curr_frame.cols, curr_frame.rows), cv::INTER_CUBIC);

    previousH = HCorrection.clone();

    //Mat frame1, frame2;
    //resize(curr_frame, frame1, Size(curr_frame.cols * resizeFactor, curr_frame.rows * resizeFactor));
    //resize(frameCorrected, frame2, Size(frameCorrected.cols * resizeFactor, frameCorrected.rows * resizeFactor));


    psnrV[frameCount] = Measurement::getPSNR(curr_frame,curr_frame_corrected);
    qDebug() << "frame " << frameCount << " of " << frameLength;
    std::cout << "\n---" << std::setiosflags(std::ios::fixed) << std::setprecision(3) << psnrV[frameCount] << "dB";

    crop(curr_frame_corrected);

    if (this->originalOutput != 0 && this->stabilizedOutput != 0){
        updateWidget(originalOutput, curr_frame);
        updateWidget(stabilizedOutput, curr_frame_corrected);
    }
    ++frameCount;

}

void VideoStabilizer::setGraphView(GraphWindow *g){
    this->graphView = g;
}

int VideoStabilizer::getFrameLength(){
    return frameLength;
}

QVector<double> VideoStabilizer::getPsnrValues(){
    return psnrV;
}

void VideoStabilizer::setDetector(QString str){
    this->detector = new GenericDetector(str);
}

void VideoStabilizer::setQuality(int quality){
    this->resizeFactor = quality/100.0;
}

void VideoStabilizer::setSpeed(int speed){
    this->speed = 2* (110 - speed);
}

void VideoStabilizer::updateWidget(QLabel *widget, cv::Mat frame){
    widget->setPixmap(QPixmap::fromImage(Utility::matToImage(frame)));
    widget->repaint();
}

void VideoStabilizer::setOriginalOutputWidget(QLabel *widget){
    this->originalOutput = widget;
}

void VideoStabilizer::setStabilizedOutputWidget(QLabel *widget){
    this->stabilizedOutput = widget;
}

VideoStabilizer::~VideoStabilizer()
{

}

