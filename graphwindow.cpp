#include "graphwindow.h"
#include "ui_graphwindow.h"

GraphWindow::GraphWindow(QWidget *parent) :
    QMainWindow(parent),
    numberOfGraphs(0),
    ui(new Ui::GraphWindow)
{
    ui->setupUi(this);
    setWindowTitle("PSNR Graph View");
    statusBar()->clearMessage();
    ui->plotWidget->replot();
    frameLength = 99;
    QColor color;
    colors.push_back(QColor(255,0,0));
    colors.push_back(QColor(0,255,0));
    colors.push_back(QColor(0,0,255));
}

void GraphWindow::addComponent(QString &name, QVector<double> yValues){
    qDebug() << "frame length = " << frameLength;
    qDebug() << "psnr value count = " << yValues.length();
    QVector<double> xValues(yValues.length());
    for (int i = 1; i <= yValues.length(); i++){
        xValues[i-1] = i;
    }


    ui->plotWidget->addGraph();
    ui->plotWidget->graph(numberOfGraphs++)->setData(xValues, yValues);
    ui->plotWidget->graph()->setName(name);
    QPen pen;
    pen.setColor(colors.at(numberOfGraphs % colors.length()));
    ui->plotWidget->graph()->setPen(pen);
    ui->plotWidget->graph()->rescaleAxes(true);
    ui->plotWidget->legend->setFont(QFont(font().family(), 7));
    ui->plotWidget->legend->setIconSize(50, 20);
    ui->plotWidget->legend->setVisible(true);
    ui->plotWidget->replot();
}

void GraphWindow::setRange(double xLower, double xUpper, double yLower, double yUpper){
    ui->plotWidget->xAxis->setRange(xLower, xUpper);
    ui->plotWidget->yAxis->setRange(yLower, yUpper);
}

void GraphWindow::setLabels(const QString x, const QString y){
    ui->plotWidget->xAxis->setLabel(x);
    ui->plotWidget->yAxis->setLabel(y);
}

GraphWindow::~GraphWindow()
{
    delete ui;
}
