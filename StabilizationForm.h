#ifndef STABILIZATIONFORM_H
#define STABILIZATIONFORM_H

#include <QMainWindow>
#include <QRadioButton>
#include "VideoStabilizer.h"

namespace Ui {
class StabilizationForm;
}

class StabilizationForm : public QMainWindow
{
    Q_OBJECT

public:
    explicit StabilizationForm(QWidget *parent = 0);
    ~StabilizationForm();

private:
    Ui::StabilizationForm *ui;
    VideoStabilizer *stabilizer;
    std::vector<QRadioButton*> *rbOptions;

private slots:
    void getVideoFile();
    void showGraphView();
    void initStabilization();

};

#endif // STABILIZATIONFORM_H
