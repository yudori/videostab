#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>
#include <QPixmap>
#include <QIcon>
#include <QProgressDialog>
#include <QVector>

#include "VideoStabilizer.h"
#include "StabilizationForm.h"
#include "ui_stabilizationform.h"
#include "graphwindow.h"

StabilizationForm::StabilizationForm(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::StabilizationForm)
{
    ui->setupUi(this);
    stabilizer = new VideoStabilizer();

    QPixmap stabPixmap(":/icons/stab.png");
    QIcon stabIcon(stabPixmap);
    ui->btnStabilize->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->btnStabilize->setIcon(stabIcon);
    ui->btnStabilize->setFixedSize(stabIcon.actualSize(stabIcon.availableSizes().first()));

    QPixmap graphPixmap(":/icons/graph.png");
    QIcon graphIcon(graphPixmap);
    ui->btnGraph->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    ui->btnGraph->setIcon(graphIcon);
    ui->btnGraph->setFixedSize(graphIcon.actualSize(graphIcon.availableSizes().first()));


    rbOptions = new std::vector<QRadioButton*>();
    rbOptions->push_back(ui->rbSurf);
    rbOptions->push_back(ui->rbSift);
    rbOptions->push_back(ui->rbOrb);

    connect(ui->btnBrowse, SIGNAL(clicked(bool)), this, SLOT(getVideoFile()));
    connect(ui->btnStabilize, SIGNAL(clicked(bool)), this, SLOT(initStabilization()));
    connect(ui->btnGraph, SIGNAL(clicked(bool)), this, SLOT(showGraphView()));

}


void StabilizationForm::getVideoFile(){
    QString file = QFileDialog::getOpenFileUrl(this, tr("Select a Video"),
                                               QUrl(),
                                               QString("Videos (*.mp4)")
                                               ).path();

    ui->leVideoLoc->setText(file);
}

void StabilizationForm::initStabilization(){

    stabilizer->setVideoPath(ui->leVideoLoc->text());
    stabilizer->setOriginalOutputWidget(ui->leftWidget);
    stabilizer->setStabilizedOutputWidget(ui->rightWidget);
    stabilizer->setQuality(ui->sldQuality->value());
    stabilizer->setSpeed(ui->sldSpeed->value());
    for (int i = 0; i < rbOptions->size(); i++){
        QRadioButton *btn = dynamic_cast<QRadioButton*>(rbOptions->at(i));
        if (btn->isChecked()){
            stabilizer->setDetector(btn->text());
            break;
        }
    }
    try{
        stabilizer->stabilize();
    }catch(const char* msg){
        QMessageBox msgBox;
        msgBox.setText(msg);
        msgBox.exec();
    }
}

void StabilizationForm::showGraphView(){

    GraphWindow *graphView = new GraphWindow();
    graphView->setRange(1,200,18,30);
    graphView->setLabels("Frame Number", "PSNR Value");
    for (int i = 0; i < rbOptions->size(); i++){
        QRadioButton *btn = dynamic_cast<QRadioButton*>(rbOptions->at(i));
        VideoStabilizer *stab = new VideoStabilizer(ui->leVideoLoc->text());
        stab->setDetector(btn->text());
        stab->setQuality(ui->sldQuality->value());
        stab->setSpeed(ui->sldSpeed->value());
        stab->setGraphView(graphView);
        stab->stabilize();
    }
    graphView->show();
    //QProgressDialog progress("Stabilizing Video, Please Wait...", "Cancel", 0, 0, this);
     //   progress.setWindowModality(Qt::WindowModal);
    //QVector<double> vals = stabilizer->stabilize();


}

StabilizationForm::~StabilizationForm()
{
    delete ui;
}


int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    StabilizationForm *sf = new StabilizationForm();
    sf->show();
    return a.exec();
}


