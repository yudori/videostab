#ifndef MEASUREMENT_H
#define MEASUREMENT_H

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>

class Measurement
{
public:
    static double getPSNR(const cv::Mat& I1, const cv::Mat& I2);

};

#endif // MEASUREMENT_H
