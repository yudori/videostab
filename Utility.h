#ifndef UTILITY_H
#define UTILITY_H

#include <QImage>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

class Utility : public QObject
{
    Q_OBJECT
public:
    explicit Utility(QObject *parent = 0);

    static QImage matToImage(cv::Mat src){
        cv::Mat temp; // make the same cv::Mat
        cv::cvtColor(src, temp,CV_BGR2RGB); // cvtColor Makes a copt, that what i need
        QImage dest((const uchar *) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
        dest.bits(); // enforce deep copy, see documentation
        // of QImage::QImage ( const uchar * data, int width, int height, Format format )
        return dest;
    }

    static cv::Mat imageToMat(QImage src){
        QImage   swapped = src.rgbSwapped();

        return cv::Mat( swapped.height(), swapped.width(),
                        CV_8UC3, const_cast<uchar*>(swapped.bits()),
                        swapped.bytesPerLine() ).clone();

    }

signals:

public slots:
};

#endif // UTILITY_H
