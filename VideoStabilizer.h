#ifndef VIDEOSTABILIZER_H
#define VIDEOSTABILIZER_H

#include <stdio.h>
#include <iostream>
#include <QString>
#include <QTimer>
#include <QLabel>
#include <QVector>
#include "opencv2/core.hpp"
#include <opencv2/core/utility.hpp>
#include "opencv2/video.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/calib3d.hpp"
#include "opencv2/videoio.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/videostab.hpp"
#include "opencv2/opencv_modules.hpp"
#include "opencv2/xfeatures2d.hpp"
#include "graphwindow.h"
#include "genericdetector.h"


class VideoStabilizer : public QObject
{
    Q_OBJECT
public:
    explicit VideoStabilizer(QString inputVideoPath = "", QObject *parent = 0);
    void setVideoPath(QString path);    
    void setQuality(int quality);
    void setSpeed(int speed);
    void setOriginalOutputWidget(QLabel *widget);
    void setStabilizedOutputWidget(QLabel *widget);
    void setDetector(QString detector);
    void setGraphView(GraphWindow *g);
    int getFrameLength();
    QVector<double> getPsnrValues();
    const char* stabilize();
    ~VideoStabilizer();

private:
    void updateWidget(QLabel *widget, cv::Mat frame);
    int frameLength;
    int frameCount;
    int speed;
    double resizeFactor;
    bool kfInitialized;
    QString detectorName;
    QVector<double> psnrV;
    QVector<double> psnrOrigninal;
    QString inputVideoPath;
    QTimer *timer;
    QLabel *originalOutput;
    QLabel *stabilizedOutput;
    cv::VideoCapture cap;
    cv::Mat prev_frame, curr_frame;
    cv::Mat prev_descriptors, curr_descriptors;
    std::vector<cv::Point2f> prev_points, curr_points;
    std::vector<cv::KeyPoint> prev_keypoints, curr_keypoints;
    std::vector<cv::DMatch> matches;
    GenericDetector *detector;
    cv::Ptr<cv::DescriptorExtractor> extractor;
    cv::Mat curr_frame_corrected;
    cv::KalmanFilter kf;
    cv::Mat previousH;
    GraphWindow *graphView;

private slots:
    void update();
};

#endif // VIDEOSTABILIZER_H
