#ifndef GENERICDETECTOR_H
#define GENERICDETECTOR_H

#include <QString>
#include <opencv2/opencv.hpp>
#include <opencv2/xfeatures2d.hpp>

class GenericDetector
{
public:
    GenericDetector(QString &name);
    QString& getName();
    cv::Ptr<cv::FeatureDetector> getDetector();
    cv::BFMatcher* getMatcher();

private:
    QString name;
};

#endif // GENERICDETECTOR_H
