#ifndef ALGORITHM_FACTORY_HPP
#define ALGORITHM_FACTORY_HPP

#include <QString>
#include <opencv2/features2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>

using FeatureDetectorPtr = cv::Ptr<cv::FeatureDetector>;

class AlgorithmFactory
{
public:
    FeatureDetectorPtr createDetector(QString&);
};


#endif  // ALGORITHM_FACTORY_HPP
